from model import Model
import torch.nn.functional as F
from utils import *
import matplotlib


def inference(original, query):
    img = preprocess_pil(original, query)

    grid, logits, dewarped_query = model(img.cuda())
    cls = torch.argmax(F.log_softmax(logits, dim=1), dim=1)
    for i, pred in enumerate(cls):
        if pred == 0:
            grid[i] *= 0

    heatmap = short_summary_image(dewarped_query.squeeze(0), prediction=grid, size=original.size)
    heatmap.show()
    print(f'The image is {model.cls2name[cls.item()]}.')


if __name__ == '__main__':
    model = Model.load_from_checkpoint('weights/best.ckpt')
    model.eval().cuda()

    original = Image.open('example/nonsquare_real.jpg').convert('RGB')
    tampered = Image.open('example/nonsquare_tampered.jpg').convert('RGB')
    # tampered_benign = Image.open('example/tampered_benign.jpg').convert('RGB')

    inference(original, original)
    inference(original, tampered)
    # inference(original, tampered_benign)

