import sys

sys.path.append('core')

import argparse
import os
import cv2
import glob
import numpy as np
import torch
from PIL import Image

from raft import RAFT
from utils import flow_viz
from utils.utils import InputPadder
from torch.autograd import Variable
import torch.nn.functional as F
import torchvision.transforms.functional as TF
import matplotlib.pyplot as plt
DEVICE = 'cuda'


def load_image(imfile):
    img = np.array(Image.open(imfile)).astype(np.uint8)
    img = torch.from_numpy(img).permute(2, 0, 1).float()
    return img[None].to(DEVICE)


def viz(img, flo):
    # img = img[0].permute(1, 2, 0).cpu().numpy()
    flo = flo[0].permute(1, 2, 0).cpu().numpy()

    # map flow to rgb image
    flo = flow_viz.flow_to_image(flo)
    img_flo = np.concatenate([img, flo], axis=0)

    # import matplotlib.pyplot as plt
    # plt.imshow(img_flo / 255.0)
    # plt.show()

    cv2.imwrite('imflo.jpg', img_flo[:, :, [2, 1, 0]] / 255.0)
    # cv2.waitKey()


def demo(args):
    model = torch.nn.DataParallel(RAFT(argparse.Namespace(alternate_corr=False, mixed_precision=False, model='models/raft-kitti.pth', path='demo-frames', small=False)))
    model.load_state_dict(torch.load(args.model))

    model = model.module
    model.to(DEVICE)
    model.eval()

    with torch.no_grad():
        images = glob.glob(os.path.join(args.path, '*.png')) + \
                 glob.glob(os.path.join(args.path, '*.jpg'))

        images = sorted(images)
        for imfile1, imfile2 in zip(images[:-1], images[1:]):
            image1 = load_image(imfile1)
            image2 = load_image(imfile2)

            padder = InputPadder(image1.shape)
            image1, image2 = padder.pad(image1, image2)

            flow_low, flow_up = model(image1, image2, iters=20, test_mode=True)
            output = warp(image2, flow_up).squeeze(0)
            print(output)
            print(torch.max(output))

            image1 = image1 / 255.0
            image2 = image2 / 255.0

            image1 = image1.contiguous()
            image2 = image2.contiguous()

            diff = grayscale_to_heatmap(TF.to_pil_image(torch.abs(image1 - output / 255).squeeze()).convert('L'))
            warped = TF.to_pil_image(output)
            im1 = TF.to_pil_image(image1.squeeze())
            im2 = TF.to_pil_image(image2.squeeze())
            fl = TF.to_pil_image(flow_viz.flow_to_image(flow_up[0].permute(1, 2, 0).cpu().numpy()))

            full = concat_h(im1, im2)
            full = concat_h(full, fl)
            full = concat_h(full, warped)
            full = concat_h(full, diff)

            warped.save('/mnt/nvme/backup_kde/PycharmProjects/psbattles/src/ph4.jpg')
            full.show()


def concat_h(im1, im2, mode=Image.BICUBIC):
    r = im1.height / im2.height
    im2 = im2.resize((int(r * im2.width), im1.height), mode)
    dst = Image.new('RGB', (im1.width + im2.width, im1.height))
    dst.paste(im1, (0, 0))
    dst.paste(im2, (im1.width, 0))
    return dst


def warp(x, flo):
    """
    warp an image/tensor (im2) back to im1, according to the optical flow

    x: [B, C, H, W] (im2)
    flo: [B, 2, H, W] flow

    """
    B, C, H, W = x.size()
    # mesh grid
    xx = torch.arange(0, W).view(1, -1).repeat(H, 1)
    yy = torch.arange(0, H).view(-1, 1).repeat(1, W)
    xx = xx.view(1, 1, H, W).repeat(B, 1, 1, 1)
    yy = yy.view(1, 1, H, W).repeat(B, 1, 1, 1)
    grid = torch.cat((xx, yy), 1).float().cuda()

    vgrid = Variable(grid) + flo
    vgrid[:, 0, :, :] = 2.0 * vgrid[:, 0, :, :].clone() / max(W - 1, 1) - 1.0
    vgrid[:, 1, :, :] = 2.0 * vgrid[:, 1, :, :].clone() / max(H - 1, 1) - 1.0

    vgrid = vgrid.permute(0, 2, 3, 1)

    output = F.grid_sample(x, vgrid, align_corners=True)

    return output

def grayscale_to_heatmap(img, cmap='jet'):
    colormap = plt.get_cmap(cmap)
    heatmap = np.array(colormap(img))
    heatmap = (heatmap * 255).astype(np.uint8)
    heatmap = Image.fromarray(heatmap)

    return heatmap

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--model', help="restore checkpoint")
    parser.add_argument('--path', help="dataset for evaluation")
    parser.add_argument('--small', action='store_true', help='use small model')
    parser.add_argument('--mixed_precision', action='store_true', help='use mixed precision')
    parser.add_argument('--alternate_corr', action='store_true', help='use efficent correlation implementation')
    args = parser.parse_args()

    demo(args)
