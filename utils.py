from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
from torchvision import transforms
import numpy as np
import torchvision.transforms.functional as TF
import matplotlib.pyplot as plt
import torch


def convert_annotation(org_size, pho_size, m_size, ann):
    """convert mturk annotation to bounding box matching original size
    org_size, pho_size, m_size are size of original, photoshop images in PSBattles and joint image shown to turker.
        Each is a tuple of (x,y) format corresponding to width and height.
    ann is mturk bounding box annotation, a tuple of format (x, y, w, h) correspond to (left, top, width, height)

    Output: (ann_out, which) where ann_out is annotation wrt natural size, which is 1 if original image is annotated,
        otherwise 0
    """
    ## forward pass
    wo, ho = org_size
    wp, hp = pho_size
    # phase 1: resize photoshop
    r = ho / hp
    wp1, hp1 = int(r * wp), ho
    # phase 2: concat
    w2, h2 = wo + wp1, ho
    # phase 3: resize to fixed width of 800
    g = 800 / w2
    w3, h3 = 800, int(h2 * g)
    # phase 4: pad text
    w4, h4 = w3, h3 + 20

    ## backward pass
    wa, ha = m_size  # mturk image size, should directly correspond to w4,h4
    # back phase 4
    a4 = (ann[0] * w4 / wa, ann[1] * h4 / ha, ann[2] * w4 / wa, ann[3] * h4 / ha)
    # back phase 3
    top = min(a4[1], h3)
    a3 = (a4[0], top, a4[2], min(a4[3] + top, h3) - top)
    # back phase 2
    a2 = (a3[0] * w2 / w3, a3[1] * h2 / h3, a3[2] * w2 / w3, a3[3] * h2 / h3)
    # back phase 1
    if a2[0] > wo:  # annotation on photoshop
        a1 = (a2[0] - wo, a2[1], a2[2], a2[3])
        which = 0
        ann_out = (a1[0] / wp1, a1[1] / hp1, a1[2] / wp1, a1[3] / hp1)
    else:  # annotation on original
        # print('Rare case: annotation on original')
        # if not a2[0] + a2[2] <= wo:
        #     print('Bounding box covers both images')
        ann_out = a2
        which = 1
    return ann_out, which


def concat_h(im1, im2, mode=Image.BICUBIC):
    r = im1.height / im2.height
    im2 = im2.resize((int(r * im2.width), im1.height), mode)
    dst = Image.new('RGB', (im1.width + im2.width, im1.height))
    dst.paste(im1, (0, 0))
    dst.paste(im2, (im1.width, 0))
    return dst


def concat_v(im1, im2, mode=Image.BICUBIC):
    r = im1.width / im2.width
    im2 = im2.resize((im1.width, int(r * im2.height)), mode)
    dst = Image.new('RGB', (im1.width, im1.height + im2.height))
    dst.paste(im1, (0, 0))
    dst.paste(im2, (0, im1.height))
    return dst


def unnormalise(y):
    # assuming x and y are Batch x 3 x H x W
    mean = [0.485, 0.456, 0.406]
    std = [0.229, 0.224, 0.225]
    x = y.new(*y.size())
    x[0, :, :] = y[0, :, :] * std[0] + mean[0]
    x[1, :, :] = y[1, :, :] * std[1] + mean[1]
    x[2, :, :] = y[2, :, :] * std[2] + mean[2]

    x = x - x.min()
    x = x / x.max()
    return x


def mask_processing(x, use_t=True):
    if use_t:
        if x > 90:
            return 140
        elif x < 80:
            return 0
        else:
            return 255
    return x


def grid_to_heatmap(grid, size, cmap='jet'):
    # TODO: pad grid with zeros to remove side stickiness ?

    mask = TF.to_pil_image(grid.view(7, 7))
    mask = mask.resize(size, Image.BICUBIC)
    mask = Image.eval(mask, mask_processing)

    # Heatmap
    colormap = plt.get_cmap(cmap)
    heatmap = np.array(colormap(mask))
    heatmap = (heatmap * 255).astype(np.uint8)
    heatmap = Image.fromarray(heatmap)

    return heatmap, mask


def grayscale_to_heatmap(img, cmap='jet'):
    colormap = plt.get_cmap(cmap)
    heatmap = np.array(colormap(img))
    heatmap = (heatmap * 255).astype(np.uint8)
    heatmap = Image.fromarray(heatmap)

    return heatmap


def summary_image(img, target, prediction):
    prediction -= prediction.min()
    prediction = prediction / prediction.max()
    size = 1024
    # Heatmap of prediction
    img1 = unnormalise(img[:, 224:, :224])
    img1 = TF.to_pil_image(img1).resize((size, size))
    heatmap, mask = grid_to_heatmap(prediction.view(7, 7).detach().numpy())
    img1.paste(heatmap, (0, 0), mask)

    # Heatmap of target
    img2 = unnormalise(img[:, :224, :224])
    img2 = TF.to_pil_image(img2).resize((size, size))
    heatmap, mask = grid_to_heatmap(target.view(7, 7).detach().numpy())
    img2.paste(heatmap, (0, 0), mask)

    target = TF.to_pil_image(target.view(7, 7))
    prediction = TF.to_pil_image(prediction.view(7, 7))

    col2 = concat_v(img1, img2)
    col1 = concat_v(prediction.resize((size, size), Image.NEAREST), target.resize((size, size), Image.NEAREST))
    full = concat_h(col1, col2)
    return full


def preprocess_pil(original, query):
    tr = transforms.Compose([transforms.Resize((224, 224)),
                             transforms.ToTensor(),
                             transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                                  std=[0.229, 0.224, 0.225]),
                             ])
    original = tr(original)
    query = tr(query)
    return torch.vstack((original, query)).unsqueeze(0)


def short_summary_image(img, size, target=None, prediction=None):
    # Photoshopped image
    if not Image.isImageType(img):
        img = unnormalise(img)
        img = TF.to_pil_image(img)
    img = img.resize(size)

    # Heatmap of target
    if target is not None:
        heatmap, mask = grid_to_heatmap(target, cmap='winter', size=img.size)
        img.paste(heatmap, (0, 0), mask)

    # Heatmap of prediction
    if prediction is not None:
        prediction -= prediction.min()
        prediction = prediction / prediction.max()
        heatmap, mask = grid_to_heatmap(prediction, cmap='Wistia', size=img.size)
        img.paste(heatmap, (0, 0), mask)

    return img


def short_summary_image_three(img, target, prediction):
    prediction -= prediction.min()
    prediction = prediction / prediction.max()

    size = 256

    # Photoshopped image
    img1 = unnormalise(img[3:, :, :])
    img1 = TF.to_pil_image(img1).resize((size, size))

    img3 = unnormalise(img[:3, :, :])
    img3 = TF.to_pil_image(img3).resize((size, size))

    img2 = img1.copy()

    # Heatmap of target
    # heatmap, mask = grid_to_heatmap(target, cmap='winter')
    # img1.paste(heatmap, (0, 0), mask)

    # Heatmap of prediction
    heatmap, mask = grid_to_heatmap(prediction, cmap='Wistia', size=size)
    img1.paste(heatmap, (0, 0), mask)

    full = concat_h(img3, img2)
    full = concat_h(full, img1)

    return full


def stn_summary_image(img, target, fake, prediction):
    prediction -= prediction.min()
    prediction = prediction / prediction.max()

    size = 1024

    # Photoshopped image
    img1 = unnormalise(img[:3, :, :])
    img1 = TF.to_pil_image(img1).resize((size, size))

    img2 = unnormalise(img[3:, :, :])
    img2 = TF.to_pil_image(img2).resize((size, size))

    img3 = unnormalise(fake)
    img3 = TF.to_pil_image(img3).resize((size, size))

    # Heatmap of target
    heatmap, mask = grid_to_heatmap(target, cmap='winter')
    img3.paste(heatmap, (0, 0), mask)

    # Heatmap of prediction
    heatmap, mask = grid_to_heatmap(prediction, cmap='Wistia')
    img3.paste(heatmap, (0, 0), mask)

    full = concat_h(img1, img2)
    full = concat_h(full, img3)

    return full


def stn_only_summary_image(img, fake):
    size = 1024

    # Photoshopped image
    img1 = unnormalise(img[:3, :, :])
    img1 = TF.to_pil_image(img1).resize((size, size))

    img2 = unnormalise(img[3:, :, :])
    img2 = TF.to_pil_image(img2).resize((size, size))

    img3 = unnormalise(fake)
    img3 = TF.to_pil_image(img3).resize((size, size))

    full = concat_h(img1, img2)
    full = concat_h(full, img3)

    return full


def dewarper_summary_image(dewarped_mask, img, warped, dewarped):
    size = 512

    img0 = unnormalise(img)
    img0 = TF.to_pil_image(img0).resize((size, size))

    img1 = unnormalise(warped)
    img1 = TF.to_pil_image(img1).resize((size, size))

    img2 = unnormalise(dewarped)
    img2 = TF.to_pil_image(img2).resize((size, size))

    img3 = TF.to_pil_image(torch.abs(unnormalise(img * dewarped_mask[0]) - unnormalise(dewarped * dewarped_mask[0]))).resize((size, size)).convert('L')
    img3 = grayscale_to_heatmap(img3)

    text_on_img(img0, 'Original')
    text_on_img(img1, 'Warped')
    text_on_img(img2, 'Dewarped')
    text_on_img(img3, 'Difference', col=(255, 255, 255))

    top = concat_h(img0, img1)
    bot = concat_h(img2, img3)

    full = concat_v(top, bot)

    return full


def text_on_img(img, text, size=24, pos=(0, 0), col=(0, 0, 0)):
    draw = ImageDraw.Draw(img)
    font = ImageFont.truetype("FreeSans.ttf", size)
    draw.text(pos, text, col, font=font)


def grid_to_binary(grid):
    mask = TF.to_pil_image(grid.view(7, 7))
    mask = mask.resize((1024, 1024), Image.BICUBIC)
    mask = Image.eval(mask, lambda x: 255 if x > 80 else 0)

    return mask


def heatmap_iou(target, prediction):
    prediction -= prediction.min()
    prediction = prediction / prediction.max()

    binary_mask_target = grid_to_binary(target)
    binary_mask_pred = grid_to_binary(prediction)

    intersection = np.count_nonzero(np.logical_and(binary_mask_target, binary_mask_pred))
    union = np.count_nonzero(np.logical_or(binary_mask_target, binary_mask_pred))

    if union == 0:
        return 0
    return intersection / union


def heatmap_emptiness(prediction):
    binary_mask_pred = grid_to_binary(prediction)

    return 1 - np.count_nonzero(binary_mask_pred) / np.array(binary_mask_pred).size
