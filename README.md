# Image Comparator
## Installation
Install new conda env with
```commandline
conda env create --file env.yml
```
**OR**

Update existing conda env with
```commandline
conda env update -n *envName* --file env.yml
```
## Running Example
```commandline
conda activate comparator
python example.py
```