import pytorch_lightning as pl
from torch import nn
from torchvision import models
import torch.nn.functional as F
from utils import *
from RAFT.core.raft import RAFT
import argparse
from torch.autograd import Variable


class Model(pl.LightningModule):
    def __init__(self, hparams):
        super().__init__()

        # Hyperparameters
        self.hparams = hparams

        self.cls2name = {0: 'not tampered', 1: 'tampered', 2: 'different'}

        self.raft = RAFT(argparse.Namespace(alternate_corr=False, mixed_precision=False, small=False))
        self.raft = torch.nn.DataParallel(self.raft)
        # self.raft.load_state_dict(torch.load('comparator/RAFT/models/raft-kitti.pth'))
        self.raft = self.raft.module
        # torch.save(self.raft.state_dict(), 'RAFT/models/raft-kitti_cpu.pth')
        # self.raft.to(self.device)
        # freeze(self.raft)

        cnn = models.resnet50(pretrained=True, progress=True)
        self.cnn_head = nn.Sequential(*list(cnn.children())[:4],
                                      *list(list(list(cnn.children())[4].children())[0].children())[:4])
        self.cnn_tail = nn.Sequential(*list(list(cnn.children())[4].children())[1:],
                                      *list(cnn.children())[5:-2])
        # freeze(self.cnn_head)  # train_bn = True
        # freeze(self.cnn_tail)  # train_bn = True

        self.conv1 = nn.Conv2d(128, 256, 3, padding=1)
        self.bn1 = nn.BatchNorm2d(num_features=256)

        self.fc1 = nn.Linear(2048 * 7 * 7, 256)
        self.fc2 = nn.Linear(256, 7 * 7)

        self.cls_fc = nn.Linear(256, 3)
        # self.sigmoid = nn.Sigmoid()

        self.criterion = nn.CrossEntropyLoss()

    def forward(self, x):
        # Input: [-1, 6, 224, 224]
        real = x[:, :3, :, :]
        fake = x[:, 3:, :, :]

        # Warp with flow estimation
        _, flo = self.raft(real, fake, iters=20, test_mode=True)
        warped_fake = self.warp(fake, flo)


        # Push both images through pretrained backbone
        real_features = F.relu(self.cnn_head(real))    # [-1, 64, 56, 56]
        fake_features = F.relu(self.cnn_head(warped_fake))    # [-1, 64, 56, 56]

        combined = torch.cat((real_features, fake_features), 1)  # [-1, 128, 56, 56]

        x = self.conv1(combined)  # [-1, 256, 56, 56]
        x = self.bn1(x)
        x = F.relu(x)

        x = self.cnn_tail(x)
        x = x.view(-1, 2048 * 7 * 7)

        # Final feature [-1, 256]
        d = F.relu(self.fc1(x))

        # Heatmap [-1, 49]
        grid = self.fc2(d)

        # Classifier [-1, 1]
        cls = self.cls_fc(d)

        # self.viz(real, fake, warped_fake, flo, grid)

        return grid, cls, warped_fake

    def run_raft(self, real, fake):
        # Warp with flow estimation
        _, flo = self.raft(real, fake, iters=20, test_mode=True)
        warped_fake = self.warp(fake, flo)
        return warped_fake

    @staticmethod
    def warp(x, flo):
        """
        warp an image/tensor (im2) back to im1, according to the optical flow

        x: [B, C, H, W] (im2)
        flo: [B, 2, H, W] flow

        """
        B, C, H, W = x.size()
        # mesh grid
        xx = torch.arange(0, W).view(1, -1).repeat(H, 1)
        yy = torch.arange(0, H).view(-1, 1).repeat(1, W)
        xx = xx.view(1, 1, H, W).repeat(B, 1, 1, 1)
        yy = yy.view(1, 1, H, W).repeat(B, 1, 1, 1)
        grid = torch.cat((xx, yy), 1).float().cuda()

        vgrid = Variable(grid) + flo
        vgrid[:, 0, :, :] = 2.0 * vgrid[:, 0, :, :].clone() / max(W - 1, 1) - 1.0
        vgrid[:, 1, :, :] = 2.0 * vgrid[:, 1, :, :].clone() / max(H - 1, 1) - 1.0

        vgrid = vgrid.permute(0, 2, 3, 1)

        output = F.grid_sample(x, vgrid, align_corners=True)

        return output
